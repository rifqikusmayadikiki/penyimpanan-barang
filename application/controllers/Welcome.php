<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function profile()
	{
	$this->load->view('profile');
	}
	public function product()
	{
	$this->load->view('product');
	}
	public function welcome_message()
	{
		$this->load->view('welcome_message');
	}
}